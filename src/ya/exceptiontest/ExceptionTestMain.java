package ya.exceptiontest;

public class ExceptionTestMain {

	public static void main(String[] args) {

		// Exempel med RuntimeException
		try {
			// Denna metod måste inte omges av try-catch eftersom MittException
			// ärver från RuntimeException och inte Exception
			metodSomKastarException(true);
		} catch (MittException e) {
			// Eftersom exceptionet catchas här så kommer programmet fortsätta
			// köras istället för att avslutas vilket sker om inte exceptionet
			// catchas
			System.out.println("Ett fel kastades!");
		} finally {
			// Detta körs alltid, oavsett om ett exception kastades eller inte
			System.out.println("Avslutar...");
		}

		// Exempel med explicit exception
		try {
			metodSomKastarExplicitException(true);
		} catch (MittExplicitaException e) {
			// Notera att denna catch måste deklareras eftersom
			// MittExplicitaException ärver från Exception
			System.out.println("Ett fel kastades här!");
		}
	}

	private static void metodSomKastarException(boolean shouldThrow) {
		if (shouldThrow) {
			throw new MittException();
		}
	}

	// Notera att MittExplicitaException måste deklareras eftersom det ärver
	// från "Exception"!
	private static void metodSomKastarExplicitException(boolean shouldThrow)
			throws MittExplicitaException {
		if (shouldThrow) {
			throw new MittExplicitaException();
		}
	}
}
