package ya.enumtest;

public class EnumTestMain {
	public static void main(String[] args) {
		genderExample();
	}

	public static void genderExample() {
		Gender gender = genderFromString("Male");

		if (gender == Gender.MALE) {
			System.out.println("Fick tillbaka en man");
		} else if (gender == Gender.FEMALE) {
			System.out.println("Fick tillbaka en kvinna");
		} else {
			System.out.println("Fick inte tillbaka någonting!");
		}

		System.out.println("Tillgängliga kön:");
		for (Gender gender2 : Gender.values()) {
			System.out.println(gender2.toString());
		}
	}

	private static Gender genderFromString(String string) {
		if ("male".equals(string.toLowerCase())) {
			return Gender.MALE;
		} else if ("female".equals(string.toLowerCase())) {
			return Gender.FEMALE;
		} else {
			return null;
		}
	}
}
